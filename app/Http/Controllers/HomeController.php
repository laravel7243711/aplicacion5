<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }
    public function descripción()
    {
        return view('home.descripción');
    }
    public function contacto()
    {
        return view('home.contacto');
    }
}
