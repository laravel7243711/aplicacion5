<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/home', [HomeController::class, 'index'])->name('home.index');
Route::get('home/descripción', [HomeController::class, 'descripción'])->name('home.descripción');
Route::get('home/contacto', [HomeController::class, 'contacto'])->name('home.contacto');